import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { UserState } from '../core/Ngrx/reducers/user.reducers';
import { loadUsers } from '../core/Ngrx/actions/user.actions';
import {
  selectTotal,
  selectUsers,
} from '../core/Ngrx/selectors/user.selectors';
import { trigger, transition, style, animate } from '@angular/animations';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [
        style({ opacity: 0, transform: 'translateY(-10%)' }),
        animate('500ms ease-out', style({ opacity: 1, transform: 'translateY(0%)' }))
      ]),
      transition(':leave', [
        animate('0s', style({ opacity: 0 }))
      ])
    ])
  ]
})
export class UsersComponent implements OnInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  isLoading: boolean = true;
  users$!: Observable<any>;
  total$!: Observable<any>;

  constructor(private router: Router, private store: Store<UserState>) {}
  ngOnInit(): void {
    this.getUsers(1, 4);
    this.users$ = this.store.pipe(select(selectUsers));
    this.total$ = this.store.pipe(select(selectTotal));
  }
  pageChange(page: PageEvent) {
    this.isLoading = true;
    this.getUsers(page.pageIndex + 1, page.pageSize);
  }
  getUsers(page: number, per_page: number) {
    this.store.dispatch(loadUsers({ page: page, perPage: per_page }));
    this.isLoading = false;
  }
  goToDetails(id: number) {
    this.router.navigate(['users', id]); //navigate to specific user details
  }
}
