import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appRoundedImage]',
  standalone: true
})
export class RoundedImageDirective {
  @Input() img!: { img_url: string };

  constructor(private elementRef: ElementRef, private renderer: Renderer2) { }

  ngOnInit() {
    this.renderer.addClass(this.elementRef.nativeElement, 'w-20');
    this.renderer.addClass(this.elementRef.nativeElement, 'h-20');
    this.renderer.addClass(this.elementRef.nativeElement, 'rounded-full');
    this.renderer.addClass(this.elementRef.nativeElement, 'overflow-hidden');
    this.renderer.addClass(this.elementRef.nativeElement, 'mb-2');

    const img = this.renderer.createElement('img');
    this.renderer.setAttribute(img, 'src', this.img.img_url);
    this.renderer.addClass(img, 'w-full');
    this.renderer.addClass(img, 'h-full');
    this.renderer.addClass(img, 'object-cover');
    this.renderer.setAttribute(img, 'alt', 'avatar');

    this.renderer.appendChild(this.elementRef.nativeElement, img);
  }
}
