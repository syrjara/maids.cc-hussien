import { Component, inject } from '@angular/core';
import { Subject, delay } from 'rxjs';
import { LoadingService } from './core/services/loading.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  loading: boolean = false;
  constructor(private _loading: LoadingService) {}
  ngOnInit(): void {
    this.listenToLoading();
  }
  listenToLoading(): void {
    this._loading.loadingSub
      .pipe(delay(0))
      .subscribe((loading) => {
        this.loading = loading;
      });
  }
}
