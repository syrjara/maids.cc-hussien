import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { UsersService } from '../core/services/users.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent {
  user: any;
  search = new FormControl();
  constructor(private userService: UsersService, private router: Router) {}
  ngOnInit(): void {
    this.search.valueChanges.subscribe((key: any) => {
      if (this.search.value != '') {
        this.userService.getUserDetails(key).subscribe({
          next: (res: any) => {
            this.user = res.data;
          },
          error: (err: any) => {
            // handling error
            this.user = null; // to prevent display user div if not found
          },
        });
      }
    });
  }
  goToDetails(id: string) {
    this.router.navigate(['/users', id]);
    this.search.setValue('');
  }
}
