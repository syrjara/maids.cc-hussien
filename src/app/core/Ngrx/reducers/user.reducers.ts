import { createReducer, on } from '@ngrx/store';
import { Users } from '../models/Users.model';
import {
  loadUsers,
  loadUsersSuccess,
} from '../actions/user.actions';

export interface UserState {
  users: Users;
  loading: boolean;
}

export const initialState: UserState = {
  users: {
    page: 0,
    per_page: 0,
    total: 0,
    total_pages: 0,
    data: [],
  },
  loading: false,
};

export const userReducer = createReducer(
  initialState,
  on(loadUsers, (state) => ({
    ...state,
    loading: true,
  })),
  on(loadUsersSuccess, (state, { users }) => ({
    ...state,
    users,
    loading: false,
  }))
);
