import { createReducer, on } from '@ngrx/store';
import { UserDetails } from '../models/user-details.model';
import {
  loadUserDetails,
  loadUserDetailsSuccess,
} from '../actions/user-details.actions';

export interface UserDetailsState {
  user: UserDetails;
  loading: boolean;
}

export const initialState: UserDetailsState = {
  user: {
    data: {
      id: 0,
      email: '',
      first_name: '',
      last_name: '',
      avatar: '',
    },
  },
  loading: false,
};

export const userDetailsReducer = createReducer(
  initialState,
  on(loadUserDetails, (state) => ({
    ...state,
    loading: true,
  })),
  on(loadUserDetailsSuccess, (state, { user }) => ({
    ...state,
    user,
    loading: false,
  }))
);
