import { createSelector, createFeatureSelector } from '@ngrx/store';
import { UserState } from '../reducers/user.reducers';

export const selectUserState = createFeatureSelector<UserState>('users');
export const selectUsers = createSelector(
  selectUserState,
  (state: UserState) => state.users.data
);

export const selectLoading = createSelector(
  selectUserState,
  (state: UserState) => state.loading
);

export const selectTotal = createSelector(
  selectUserState,
  (state: UserState) => state.users.total
);
