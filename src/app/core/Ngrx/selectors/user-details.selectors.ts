import { createFeatureSelector, createSelector } from '@ngrx/store';
import { UserDetailsState } from '../reducers/user-details.reducer';

export const selectUserDetailsState = createFeatureSelector<UserDetailsState>('userDetails');

export const selectUserDetails = createSelector(
  selectUserDetailsState,
  state => state.user.data
);

export const selectUserDetailsLoading = createSelector(
  selectUserDetailsState,
  state => state.loading
);
