import { createAction, props } from '@ngrx/store';
import { Users } from '../models/Users.model';

export const loadUsers = createAction('[User] Load Users', props<{ page: number, perPage: number }>());
export const loadUsersSuccess = createAction('[User] Load Users Success', props<{ users: Users }>());
