import { createAction, props } from '@ngrx/store';
import { User, UserDetails } from '../models/user-details.model';

export const loadUserDetails = createAction('[User Details] Load User Details', props<{ userId: number }>());
export const loadUserDetailsSuccess = createAction('[User Details] Load User Details Success', props<{ user: UserDetails }>());
