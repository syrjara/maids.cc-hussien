import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { map, mergeMap } from 'rxjs/operators';
import {
  loadUserDetails,
  loadUserDetailsSuccess,
} from '../actions/user-details.actions';
import { UsersService } from '../../services/users.service';

@Injectable()
export class UserDetailsEffects {
  loadUserDetails$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadUserDetails),
      mergeMap(({ userId }) =>
        this.userService
          .getUserDetails(userId)
          .pipe(map((user) => loadUserDetailsSuccess({ user })))
      )
    )
  );

  constructor(private actions$: Actions, private userService: UsersService) {}
}
