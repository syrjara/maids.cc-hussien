import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { mergeMap, map } from 'rxjs/operators';
import {loadUsersSuccess,loadUsers} from '../actions/user.actions';
import { UsersService } from '../../services/users.service';

@Injectable()
export class UserEffects {
  loadUsers$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadUsers),
      mergeMap(({page,perPage}) =>
        this.userService.getUsers(page,perPage).pipe(
          map(users => loadUsersSuccess({ users })),
        )
      )
    )
  );

  constructor(private actions$: Actions, private userService: UsersService) {}
}
