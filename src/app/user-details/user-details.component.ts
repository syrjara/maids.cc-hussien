import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserDetailsState } from '../core/Ngrx/reducers/user-details.reducer';
import { Store, select } from '@ngrx/store';
import {
  selectUserDetails,
  selectUserDetailsLoading,
} from '../core/Ngrx/selectors/user-details.selectors';
import { loadUserDetails } from '../core/Ngrx/actions/user-details.actions';
import { trigger, transition, style, animate } from '@angular/animations';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [
        style({ opacity: 0, transform: 'translateX(100%)' }),
        animate('500ms ease-out', style({ opacity: 1, transform: 'translateX(0%)' }))
      ]),
      transition(':leave', [
        animate('0s', style({ opacity: 0 }))
      ])
    ])
  ]
})
export class UserDetailsComponent implements OnInit {
  user_details: any;
  user_details$!: Observable<any>;
  isLoading$!: Observable<boolean>;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<UserDetailsState>
  ) {}
  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      const id = params['id'];
      this.store.dispatch(loadUserDetails({ userId: id }));
      this.user_details$ = this.store.pipe(select(selectUserDetails));
      this.isLoading$=this.store.pipe(select(selectUserDetailsLoading))
    });
  }
  back() {
    this.router.navigate(['/users']);
  }
}
